const { I } = inject();

module.exports = {
    // setting 
  time: {
    shortTimeOut: '30',
    longTimeOut: '60'
  },

  // insert your locators and methods here

   formatXpath(xpath, ...args) {
    if (!xpath.match(/^(?:(?:(?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{[0-9]+\}))+$/)) {
        throw new Error('invalid format string.');
    }
    return xpath.replace(/((?:[^{}]|(?:\{\{)|(?:\}\}))+)|(?:\{([0-9]+)\})/g, (m, str, index) => {
        if (str) {
            return str.replace(/(?:{{)|(?:}})/g, m => m[0]);
        } else {
            if (index >= args.length) {
                throw new Error('argument index is out of range in format');
            }
            return args[index];
        }
    });
},
  
  fillToField(xpath, value){
    I.waitForElement(xpath, this.time.shortTimeOut);
    I.fillField(xpath, value);
  },

  fillToField(xpath, value, ...args){
    xpath = this.formatXpath(xpath, ...args);
    I.waitForElement(xpath, this.time.shortTimeOut);
    I.fillField(xpath, value);
  },

  tapToButton(xpath){
    I.waitForElement(xpath, this.time.shortTimeOut);
    I.tap(xpath);
  },

 async isElementDisplayed(xpath){
    I.waitForElement(xpath, this.time.longTimeOut);
    const value = await I.grabNumberOfVisibleElements(xpath);
    return value;
  },

  verifyTrue(condition){
    const pass = true;
    try {
        if(condition == true){
            console.log('---Pass---');
        }
        else
        {
            console.log('---Failed---');
        }
    } catch (error) {
        pass = false;
    }
    return pass;
  }

}