const { I } = inject();

const commonSteps = require('../steps/commons.js');
const loginMobilePage = require('../pages/login_mobile.js')
module.exports = {

  // insert your locators and methods here
  login(qrCode, password, otpCode){

    // if (commonSteps.isElementDisplayed(loginMobilePage.login.laterButton)) {
    //   commonSteps.tapToButton(loginMobilePage.login.laterButton);
    // }
    commonSteps.tapToButton(loginMobilePage.login.skipButton);

    for(let i = 0; i < qrCode.length; i++)
    {
      commonSteps.fillToField(loginMobilePage.login.codeDynamic, qrCode.charAt(i), i);
    }

    
    commonSteps.fillToField(loginMobilePage.login.passWord, password);

    commonSteps.tapToButton(loginMobilePage.login.signUpButton);

    for(let i = 0; i < otpCode.length; i++){
      commonSteps.fillToField(loginMobilePage.login.otpDynamic, otpCode.charAt(i), i);
    }
  
    commonSteps.verifyTrue(commonSteps.isElementDisplayed(loginMobilePage.login.userProfile));
  }

}
