const { I } = inject();

module.exports = {
  login: {
    laterButton : '//android:id/button1',
    skipButton : '//android.view.ViewGroup[@content-desc="tutorial_skip"]',
    codeDynamic :  '//android.widget.EditText[@content-desc="activation_{0}"]',    
    username : '//android.widget.EditText[@content-desc="login_username"]',
    passWord : '//android.widget.EditText[@content-desc="login_password"]',
    signUpButton: '//android.view.ViewGroup[@content-desc="login_signIn"]',
    otpDynamic: '//android.widget.EditText[@content-desc="otp_{0}"]',
    userProfile: '//android.view.ViewGroup[@content-desc="bottomTab_settings"]/android.view.ViewGroup/android.widget.ImageView',
    retryButton: '//android.view.ViewGroup[@content-desc="retry"]'
  }
}
